import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Navegação', url: 'pages/navegacao', icon: 'paper-plane' },
    { title: 'Navegação2', url: 'pages/navegacao2', icon: 'paper-plane' },
    { title: 'Botões', url: 'pages/botoes', icon: 'shapes' },
    { title: 'Alertas', url: 'pages/alerts', icon: 'shapes' },
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor() {}
}
